﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Animator bagButton;
    public Animator mapButton;
    public Image cooldown;
    public bool coolingDown;
    public float waitTime = 30.0f;

    void Update()
    {
        if (coolingDown == true && cooldown.fillAmount > 0)
        {
            print("Decrease!");
            //Reduce fill amount over 30 seconds
            cooldown.fillAmount -= 1.0f / waitTime * Time.deltaTime;
            print("fill:" + cooldown.fillAmount);
            if(cooldown.fillAmount <= 0f)
            {
                coolingDown = false;
            }
        }
    }
    public void CoolDownClick()
    {
        print("called!");
        if (!coolingDown)
        {
            print("Started!");
            coolingDown = true;
            cooldown.fillAmount = 1;
        }
    }
    public void OpenMap()
    {
        mapButton.SetBool("isHidden", true);
    }

    public void OpenBag()
    {
        bagButton.SetBool("isHidden", true);
    }

    public void CloseBag()
    {
        bagButton.SetBool("isHidden", false);
    }
    public void CloseMap()
    {
        mapButton.SetBool("isHidden", false);
    }
}

